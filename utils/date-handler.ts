
/**
 * Returns a date un European fromat
 * @param {String} date Date in US format
 */
export function dateUStoEU(date){
    if (!date) return null
    var tmp = date.split(`-`)
    return (tmp.length == 3) ? `${tmp[2]}/${tmp[1]}/${tmp[0]}` : date
}