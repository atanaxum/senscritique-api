import { Action, Filter, Type } from "./enums";

export interface CollectionOptions{
    action? : Action, 
    type?   : Type, 
    filter? : Filter,
    page?   : number,
}
export interface CollectionItem{
    title       : string,
    type        : string,
    genres      : string[],
    rating      : number,
    member      : string,
    recommend   : boolean,
    release_date: string,
    creators    : string[],
    url         : string,
}
export interface CollectionResponse{
    items       : CollectionItem[],
    rejection?  : string,
    total_pages : number,
    base_url    : string,
    query       : string,
    params      :{ 
        member  : string, 
        type    : string, 
        action  : string, 
        filter  : string, 
        page    : number
    }
}


export interface Entry{
    title   : string,
    type    : string,
    creators: string[],
    year    : number | null,
    genres  : string[],
    rating  : number,
    url     : string,
}
export interface SearchResponse{
    items   : Entry[],
    total   : number,
    base_url: string,
    query   : string,
    params  : { 
        q   : string, 
        p   : number 
    }
}


export interface Ratings{
    rating?     : number, 
    recommend?  : number, 
    wish?       : number, 
    current?    : number 
}
export interface Versus{ 
    author      : string, 
    rating      : number, 
    title       : string, 
    description : string, 
    url         : string 
}
export interface Details{
    title   : string,
    cover   : string,
    rating  : number,
    rating_details: Ratings,
    resume  : string,
    type    : string,
    creators: string[],
    genres  : string[],
    date    : string,
    tracks? : string[],
    casting?: string[],
    url     : string,
    versus? : Versus[]
}


export interface TopEntry{
    podium  : number,
    title   : string,
    year    : number 
}
export interface MemberInfo{
    member      : string,
    avatar      : string,
    description : string,
    followers   : number,
    following   : number,
    rates       : number,
    links       : {
        label   : string, 
        url     : string }[],
    stats       : {
        category: string, 
        value   : number, 
        url     : string }[],
    top10       : {
        title   : string, 
        top     : TopEntry[] }[],
    url         :string
}