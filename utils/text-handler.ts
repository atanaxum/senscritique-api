/**
 * Removes \n and \t characters from a string
 * @param {String} text Any text
 */
export function cleanText(text: string){
    return text.split(/\n|\t/).join(``).trim()
}

/**
 * Makes first character of a string upper case
 * @param {String} s Any string
 */
export function capitalize(s: string){
    if(s.length > 0) return s[0].toUpperCase() + s.slice(1);
}

/**
 * Transforms a stringified version of a number into a number
 * @param {String} quantity A string expressing a number ("1.4K" is an acceptable format)
 */
export function stringToNumber(quantity: string) : number{
    var tmp = quantity.replace(`K`, ``)
    var thousand = (tmp.length < quantity.length)
    return (thousand) ? parseFloat(quantity) * 1000 : parseInt(quantity)
}