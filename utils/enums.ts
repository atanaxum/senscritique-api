
export enum Action {
    ALL         = `all`,
    WISH        = `wish`,
    RECOMMEND   = `recommend`,
    CURRENT     = `current`,
    RATING      = `rating`,
    DONE        = `done`
}

export enum Type {
    ALL     = `all`,
    FILM    = `films`,
    SERIE   = `series`,
    GAME    = `jeuxvideo`,
    BOOK    = `livres`,
    COMICS  = `bd`,
    ALBUM   = `albums`,
    TRACK   = `morceaux`
}

export enum Filter {
    ALL         = `all`,
    DATE        = `release_date`,
    USER        = `user_rating`,
    RATING      = `global_rating`,
    POPULARITY  = `popularite`,
    ALPHABETIC  = `titre`
}