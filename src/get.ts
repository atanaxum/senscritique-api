import axios from "axios"
import * as cheerio from "cheerio"
import { capitalize, cleanText, stringToNumber } from "../utils/text-handler"
import { dateUStoEU } from "../utils/date-handler"
import { Action } from "../utils/enums"
import { base_url } from "../config"
import { Details, Ratings, Versus } from "../utils/typedef"



/**
 * Return a JSON version of a page
 * @param {String} url An url of a movie/book/album from SensCritique
 */
export async function get(url: string) : Promise<Details>{
    let result : Details = null
    return axios.get(url).then(response => {
        if(response.status == 200){
            const $ = cheerio.load(response.data)
            let body = $(`div.wrap__content`)

            let rating_details : Ratings = {}
            body.find(`.pvi-scrating-details`).children().each((i,e) => {
                let category = categoryName($(e).attr(`title`))
                if(category){
                    rating_details[category] = stringToNumber($(e).find(`b`).text())
                }
            })

            let details = body.find(`.pvi-productDetails`)
            let genres : string[] = []
            details.find(`[itemprop="genre"]`).each((i,e) => {
                genres.push(capitalize($(e).text()))
            })
            let creators : string[] = []
            details.find(`span a`).each((i,e) => {
                creators.push($(e).text())
            })

            let raw_versus = body.find(`.ere-review`)
            let versus : Versus[] = []
            raw_versus.find(`article`).each((i,e) => {
                let review_url = $(e).find(`.ere-review-excerpt a`).attr(`href`)
                $(e).find(`.ere-review-excerpt a`).remove()
                versus.push({
                    author      : cleanText($(e).find(`.d-offset`).text()),
                    rating      : parseInt( cleanText($(e).find(`.elrua-useraction-action`).text()) ),
                    title       : cleanText($(e).find(`.ere-review-heading`).text()),
                    description : cleanText($(e).find(`.ere-review-excerpt`).text()),
                    url         : base_url + review_url
                })
            })

            let type : string = null
            body.find(`.lahe-breadcrumb-anchor`).each((i,e) => {
                if(i==1) type = $(e).text()
            })
            
            body.find(`.pvi-productDetails-resume button`).remove() //Removes "Lire la suite" and "Fermer" from resume
            result = {
                title   : cleanText( body.find(`.pvi-product-title`).text() ),
                cover   : body.find(`.pvi-hero-poster`).attr(`src`),
                rating  : parseInt( body.find(`.pvi-scrating-value`).text() ),
                rating_details,
                resume  : cleanText( body.find(`.pvi-productDetails-resume`).text() ),
                type,
                creators,
                genres,
                date    : dateUStoEU(details.find(`time`).attr(`datetime`)),
                url,
                versus
            }

            if(type == `Musique`){
                let track : string[] = []
                body.find(`.elt-tracks-item`).each((i,e) => {
                    track.push($(e).find(`[itemprop="name"]`).text())
                })
                result.tracks = track
            } else if (type == `Films` || type == `Séries`){
                let cast : string[] = []
                body.find(`.ecot-contact-caption`).each((i,e) => {
                    cast.push($(e).find(`span`).first().text())
                })
                result.casting = cast
            }
        }
        return result 
    }).catch( err => {
        console.error(err)
        return result 
    } )
}


function categoryName(keywords: string){
    switch (keywords.toLowerCase()) {
    case `notes`            : return Action.RATING
    case `coups de coeur`   : return Action.RECOMMEND
    case `envies`           : return Action.WISH
    case `en cours`         : return Action.CURRENT
    }
    return null
}