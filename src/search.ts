import axios from "axios"
import * as cheerio from "cheerio"
import { capitalize } from "../utils/text-handler"
import { base_url } from "../config"
import { Entry, SearchResponse } from "../utils/typedef"

/**
 * Search something on SensCritique
 * @param {String} keywords Something to search on senscritique
 * @param {Number} page Page index (default = 1)
 */
export async function search(keywords : string, page : number = 1) : Promise<SearchResponse>{
    let result : SearchResponse = {
        items       : [],
        total       : 0,
        base_url    : `${base_url}/search`,
        query       : `${base_url}/search?q=${keywords}&p=${page}`,
        params      : {
            q : keywords,
            p : page,
        }
    }
    return axios.get(result.query).then(response => {
        if(response.status == 200){
            const $ = cheerio.load(response.data)
            result.total = parseInt($(`.sk-hits-stats__info`).text().split(` `).join(``))

            let search_body = $(`div.sk-hits`)
            search_body.children().each((i,e) => {
                let genres : string = $(e).find(`ul`).text()
                let desc : string[] = $(e).find(`div[class*="Text"] p`).text().split(`-`)
                let tmp : string[] =  (desc[0].length > 0) ? desc[0].split(`de`) : []
                let entry : Entry = {
                    title   : $(e).find(`h3`).text(),
                    type    : (tmp.length > 1) ? tmp[0].trim() : null,
                    creators: (tmp.length > 1) ? tmp[1].split(`,`).map(name => name.trim()) : [],
                    year    : (desc[1]) ? parseInt( desc[1] ) : null,
                    genres  : (genres.length == 0) ? [] : genres.split(`,`).map((genre) => capitalize(genre.trim())),
                    rating  : parseInt( $(e).find(`div[class*="RatingGlobal"] span`).first().text() ),
                    url     : $(e).find(`div[class*="Text"] a`).attr(`href`)
                }
                result.items.push(entry)
            })
        }
        return result 
    }).catch( err => {
        console.error(err)
        return result 
    } )
}