import axios  from "axios"
import * as cheerio from "cheerio"
import { base_url } from "../config"
import { cleanText } from "../utils/text-handler"
import { MemberInfo, TopEntry } from "../utils/typedef"


/**
 * Retrieve information about a user of Senscritique
 */
export async function member(username: string) : Promise<MemberInfo>{
    const url : string = `${base_url}/${username}`
    let result : MemberInfo = null
    return axios.get(url).then(response => {
        if(response.status == 200){
            const $ = cheerio.load(response.data)

            let links : {label : string, url : string}[] = []
            $(`a.uco-cover-rellink`).each((i,e) => {
                links.push({
                    label   : $(e).text(),
                    url     : $(e).attr(`href`)
                })
            })

            let followers : number = 0
            let following : number = 0
            let rates : number = 0
            $(`.uvi-numbers-item`).each((i,e) => {
                var value = $(e).find(`.uvi-numbers-count`).text() 
                $(e).find(`.uvi-numbers-count`).remove()
                switch ($(e).text().trim()) {
                case `éclaireurs`   : following = parseInt(value); break
                case `abonnés`      : followers = parseInt(value); break
                case `notes`        : rates = parseInt(value);     break
                }
            })

            let stats : {category: string, value: number, url: string}[] = []
            $(`.uvi-stats-pies`).children().each((i,e) => {
                if($(e).attr(`data-sc-pie-label`)){
                    stats.push({
                        category: $(e).attr(`data-sc-pie-label`),
                        value   : parseInt( $(e).attr(`data-sc-pie-value`) ),
                        url     : base_url + $(e).attr(`href`)
                    })
                }
            })

            let ranking : {title : string, top : TopEntry[]}[] = []
            $(`.uvi-ranking`).each((i, e) => {
                let title : string = $(e).find(`.uvi-ranking-title`).text()
                let list : TopEntry[] = []
                $(e).find(`.uvi-ranking-item`).each((ii, ee) => {
                    list.push({
                        podium  : ii + 1,
                        title   : $(ee).find(`.uvi-ranking-anchor`).text(),
                        year    : parseInt( $(ee).find(`.uvi-ranking-release`).text().split(/\(|\)/).join(``) )
                    })
                })
                ranking.push({title, top : list})
            })
            
            result = {
                member      : $(`.d-cover-title`).text(),
                avatar      : $(`img.uco-cover-avatar`).attr(`src`),
                description : cleanText( $(`.uvi-resume-description`).text() ),
                followers,
                following,
                rates,
                links,
                stats,
                top10 : ranking,
                url,
            }
        }
        return result 
    }).catch( err => {
        console.error(err)
        return result 
    } )
}