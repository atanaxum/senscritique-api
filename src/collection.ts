import axios from "axios"
import * as cheerio from "cheerio"
import { Action, Type, Filter } from "../utils/enums"
import { dateUStoEU } from "../utils/date-handler"
import { capitalize, cleanText } from "../utils/text-handler"
import { base_url } from "../config"
import { CollectionItem, CollectionOptions, CollectionResponse } from "../utils/typedef"


const options : CollectionOptions = {
    action  : Action.ALL, 
    type    : Type.ALL, 
    filter  : Filter.ALL,
    page    : 1,
}

/**
 * Returns a list of objects from the collection of a user
 * @param {String} member Name of the user
 */
export async function collection(member: string, opts : CollectionOptions = options) : Promise<CollectionResponse>{
    let result : CollectionResponse = {
        items       : [],
        total_pages : 0,
        base_url    : `${base_url}/${member}/collection`,
        query       : getUrl(member, opts),
        params      : {
            member,
            type    : (opts.type)   ? opts.type     : options.type,
            action  : (opts.action) ? opts.action   : options.action,
            filter  : (opts.filter) ? opts.filter   : options.filter,
            page    : (opts.page)   ? opts.page     : options.page,
        }
    }

    return axios.get(result.query).then(response => {
        if(response.status == 200){
            const $ = cheerio.load(response.data)
            var body = $(`.elco-collection-list`)

            if(body.text().length == 0){
                var msg = $(`.uvi-restricted p`).text()
                result.rejection = msg
                return result
            }

            var pages = $(`.eipa-pages a`).last().attr(`data-sc-pager-page`)
            if(pages && pages.length > 0) result.total_pages = parseInt(pages)

            body.children().each((i,e) => {
                var user = $(e).find(`.elrua-useraction-inner`)
                var wish = user.find(`.eins-wish-list`).attr()
                var creators = []
                $(e).find(`.elco-baseline-a`).each((j,el) => {
                    creators.push($(el).text())
                })

                var type = null
                $(e).find(`.elco-baseline`).each((j,el) => {
                    if(j==1) {
                        var tmp = cleanText($(el).text())
                        type = tmp.split(`de`)[0].trim()
                    }
                })

                var genres : string[] = null
                var genres_tmp : string[] = cleanText( $(e).find(`.elco-options`).text() ).split(`.`)
                if(genres_tmp.length >= 2){
                    genres = genres_tmp[genres_tmp.length-2].split(/,|et/)
                    genres = genres.map(x => capitalize( cleanText(x) ))
                }

                var like = $(e).find(`[class*="recommend"]`).attr()
                var entry : CollectionItem = {
                    title       : $(e).find(`a[id*="product-title"]`).text(),
                    rating      : parseFloat( cleanText( $(e).find(`.erra-global`).text() ) ),
                    type,
                    genres,
                    member      : (wish) ? Action.WISH : cleanText( user.text() ),
                    recommend   : (like != null),
                    release_date: dateUStoEU($(e).find(`.elco-options time`).attr(`datetime`)),
                    creators,
                    url         : base_url + $(e).find(`a[id*="product-title"]`).attr(`href`)
                }
                result.items.push(entry)
            })
        }
        if(result.total_pages == 0){
            result.items = []
        }
        return result
    }).catch( err => {
        console.error(err)
        return result 
    } )
}


/**
 * @param {String} member Name of the user
 */
function getUrl(member, {action = Action.ALL, type = Type.ALL, filter = Filter.ALL, page = 1}){
    return `${base_url}/${member}/collection/${action}/${type}/${filter}/page-${page}`
}