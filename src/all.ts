import { collection } from "./collection"
import { Action, Type } from "../utils/enums"
import { CollectionItem, CollectionOptions } from "../utils/typedef"

async function prom(member: string, options : CollectionOptions){
    return new Promise((resolve) => {
        collection(member, options).then(response => {
            resolve(response.items)
        })
    })
}

/**
 * Returns entire collection of a users
 * @param { string } member Name of a user
 * @param { Action } action 
 * @param { Type } type 
 */
export async function all(member: string, action = Action.ALL, type = Type.ALL) : Promise<CollectionItem[]>{
    let response : CollectionItem[] = []
    let promises = []
    var first = await collection(member, { action, type, page: 1})
    response = response.concat(first.items)
    var pages = first.total_pages

    for(var i = 2; i <= pages; i++ ){
        promises.push(prom(member, { action, type, page: i}))
    }

    return (Promise as any).allSettled(promises).then(results => {
        results.forEach(res => {
            response = response.concat(res.value)
        })
        return response
    })
}