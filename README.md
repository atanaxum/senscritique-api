# @nyakimov/senscritique-api
<p align="center">
  <a href="https://github.com/miramo/senscritique-api">
    <img  alt="SensCritique Logo" 
          src="https://upload.wikimedia.org/wikipedia/commons/c/cc/Logo_SensCritique_.png" 
          width="350">
  </a>
</p>
An unofficial API for Senscritique that helps you to retrieve informattion about movies, books, video games and tv shows! (using web scrapping)

## Import
Import the library using `require`
```js
const sc = require('@nyakimov/senscritique-api')
```

-----------

## search
Call `search` with some keywords to display 16 results
```js
const sc = require('@nyakimov/senscritique-api')

var page = 1 //Optional (default = 1)
sc.search(`The witcher`, page).then(res => {
    console.log(res)
})

Result {
    items: [{
        title: 'The Witcher',
        type: 'Série',
        creators: [ 'Lauren Schmidt Hissrich' ],
        year: ' 2019',
        genres: [ 'Fantasy', 'Aventure' ],
        rating: '6.5',
        url: 'https://www.senscritique.com/serie/...'
    },
    ...
  ],
  total: 722571,
  base_url: 'https://www.senscritique.com/search',
  query: 'https://www.senscritique.com/search?q=The witcher&p=1',
  params: { q: 'The witcher', p: 1 }
}
```


## get
Call `get` with the url of a movie/book/tvshow to get detailed informations
```js
const sc = require('@nyakimov/senscritique-api')

sc.get(`https://www.senscritique.com/jeuvideo/The_Witcher/85891`).then(res => {
    console.log(res)
})

Result {
  title: 'The Witcher',
  cover: 'https://media.senscritique.com/media/...',
  rating: '7.4',
  ...
}
```


## collection
Call `collection` with the name of a member and filter options (optional)
```js
const sc = require('@nyakimov/senscritique-api')

sc.collection(`radegast`, {type : Type.FILM}).then(res => {
    console.log(res)
})

Result {
  items: [
    {
      title: 'Melancholia',
      rating: '7.0',
      member: 'wish',
      date: '26/05/2011',
      creators: [ 'Lars von Trier' ]
    },
    ...
  ],
  base_url: 'https://www.senscritique.com/radegast/collection',
  query: 'https://www.senscritique.com/radegast/collection/all/all/all/page-1',
  params: {
    member: 'radegast',
    type: 'all',
    action: 'all',
    filter: 'all',
    page: 1
  }
}
```


## all
Call `all` with the name of a member to get all their rating/wishes/recommendations (equivalent of doing `collection` for all pages available)
```js
const sc = require('@nyakimov/senscritique-api')

//Export all records of radegast
sc.all(`radegast`).then(res => {
    console.log(res)
})

//Export entire list of wishes of radegast
sc.all(`radegast`, Action.WISH).then(res => {
    console.log(res)
})

//Export entire list of games of radegast
sc.all(`radegast`, Action.ALL, Type.GAME).then(res => {
    console.log(res)
})
```
/!\ Can return an empty list if the user has defined his profile as private



## member
Call `member` with the name of a member to get global information about a user of Senscritique
```js
const sc = require('@nyakimov/senscritique-api')

//Export all records of radegast
sc.member(`radegast`).then(res => {
    console.log(res)
})

Result {
    member : `Radegast`,
    avatar : `https://media.senscritique.com/media/...`
    ...
}
```

-----------

## Object returned by `get` function

```js
Get Result {
    title: String,
    cover: String,
    rating: String,
    rating_details: { 
        rating: Number,
        recommend: Number, 
        wish: Number, 
        current: Number 
    },
    resume: String,
    type: String,
    creators: [...String],
    genres: [...String],
    date: String,
    versus: [{
        author: String, //Person with a positive opinion
        rating: String,
        title: String,
        description: String,
        url: String
    },{
        author: String, //Person with a negative opinion
        rating: String,
        title: String,
        description: String,
        url: String
    }
}
```

## Object returned by `member` function

```js
Member Result {
    member: String,
    avatar: String,
    description: String,
    followers: Number,
    following: Number, 
    rates: Number, 
    links: [...{
        label : String,
        url   : String
    }],
    stats: [...{
        category: String,
        value: Number,
        url: String
    }],
    top10: [...{
        title: String,
        top: [...{
            podium: Number,
            title: String,
            year: Number
        }]
    }],
    url: String
}
```

## Options available for `collection` function
Defaut option:
```js
var default_options = {
    action  : Action.ALL, 
    type    : Type.ALL, 
    filter  : Filter.ALL,
    page    : 1,
}
sc.search(`something`, default_options).then(result => ...)
```

### action (also works in `all` function)
- `Action.ALL` _(default)_
- `Action.WISH`     : member's wishes
- `Action.RECOMMEND`: member's favorites
- `Action.CURRENT`  : member is currently watching/reading/playing
- `Action.RATING`   : member was rated
- `Action.DONE`     : what is not in current

### type
- `Type.ALL` _(default)_
- `Type.FILM`
- `Type.SERIE`
- `Type.GAME`
- `Type.BOOK`
- `Type.COMICS`
- `Type.ALBUM`
- `Type.TRACK`

### filter
- `Filter.ALL` _(default)_
- `Filter.DATE`     : sort by release date
- `Filter.USER`     : sort by member's rating
- `Filter.RATING`   : sort by global rating
- `Filter.POPULARITY`
- `Filter.ALPHABETIC`